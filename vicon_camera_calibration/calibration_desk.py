import numpy as np


class CalibrationDesk(object):
    def __init__(self, desk_shape=(10, 7), cell_size=0.015, offset_x=0.0123 + 10 * 0.015, offset_y=0.0432,
                 offset_z=0.0082):
        self._desk_corners = self._get_corners(desk_shape, cell_size, offset_x, offset_y, offset_z)

    def get_global_corners(self, timestamp):
        return self._desk_corners

    @staticmethod
    def _get_corners(desk_shape, cell_size, offset_x, offset_y, offset_z):
        x, y = np.meshgrid(range(desk_shape[0]), range(desk_shape[1]))
        y = y.reshape(-1) * cell_size + offset_y
        x = -x.reshape(-1) * cell_size + offset_x
        z = np.zeros_like(x) + offset_z
        return np.stack([x, y, z], axis=1)
