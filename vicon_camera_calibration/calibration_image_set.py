import cv2
import numpy as np


class CalibrationImageSet(object):
    def __init__(self, images, timestamps, desk_shape=(10, 7)):
        self._images = images
        self._timestamps = timestamps
        self._desk_shape = desk_shape

    def get_image(self, timestamp):
        index = np.searchsorted(self._timestamps, timestamp)
        if index > len(self._images) - 1:
            index = len(self._images) - 1
        return self._images[index]

    def get_local_corners(self, timestamp):
        image = self.get_image(timestamp)
        ret, corners = cv2.findChessboardCorners(image, self._desk_shape,
                                                 flags=cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_EXHAUSTIVE)
        if not ret:
            raise ValueError("[CalibrationImageSet] ret is False")
        return corners
