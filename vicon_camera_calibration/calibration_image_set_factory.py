import rosbag
from cv_bridge import CvBridge

from .calibration_image_set import CalibrationImageSet


class CalibrationImageSetFactory(object):
    @staticmethod
    def make_calibration_image_set(rosbag_file_path, topic):
        bagfile = rosbag.Bag(rosbag_file_path)
        images = []
        timestamps = []
        for topic, message, t in bagfile.read_messages(topics=[topic]):
            images.append(CalibrationImageSetFactory.image_from_message(message))
            timestamps.append(t.to_sec())
        bagfile.close()
        return CalibrationImageSet(images, timestamps)

    @staticmethod
    def image_from_message(message):
        bridge = CvBridge()
        cv_image = bridge.imgmsg_to_cv2(message)
        return cv_image
