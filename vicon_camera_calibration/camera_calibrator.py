import numpy as np


class CameraCalibrator(object):
    def __init__(self, calibration_desk, calibration_image_set, world_from_desk_transform, setup_from_world_transform,
                 calibration_algorithm):
        self._calibration_desk = calibration_desk
        self._calibration_image_set = calibration_image_set
        self._world_from_desk_transform = world_from_desk_transform
        self._setup_from_world_transform = setup_from_world_transform
        self._calibration_algorithm = calibration_algorithm

    def calibrate(self):
        timestamps = self._get_timestamps()
        full_global_corners = np.zeros((0, 3))
        full_local_corners = np.zeros((0, 2))
        for timestamp in timestamps:
            global_corners = self._get_global_corners(timestamp)
            full_global_corners = np.concatenate([full_global_corners, global_corners])
            local_corners = self._calibration_image_set.get_local_corners(timestamp)
            full_local_corners = np.concatenate([full_local_corners, local_corners])
        return self._calibration_algorithm.calibrate(full_global_corners, full_local_corners)

    def _get_global_corners(self, timestamp):
        global_corners = self._calibration_desk.get_global_corners(timestamp)
        global_corners = self._world_from_desk_transform.transform(global_corners, timestamp)
        global_corners = self._setup_from_world_transform.transform(global_corners, timestamp)
        return global_corners

    def _get_timestamps(self):
        return []
