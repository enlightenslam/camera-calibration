import numpy as np

from .math import transform_global_from_local


class FrameArray(object):
    def __init__(self, positions, timestamps):
        self._timestamps = timestamps
        self._positions = positions

    def get_frame(self, timestamp):
        index = np.searchsorted(self._timestamps, timestamp)
        if index > len(self._positions) - 1:
            index = len(self._positions) - 1
        return self._positions[index]

    def transform(self, points, timestamp):
        frame = self.get_frame(timestamp)
        return transform_global_from_local(frame, points)
