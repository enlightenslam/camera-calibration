import numpy as np


def transform_global_from_local(transform, positions):
    if len(transform.shape) == 2 and transform.shape == (3, 4) and len(positions.shape) == 2 and \
            positions.shape[-1] == 3:
        return positions @ transform[:3, :3].T + transform[:3, 3]
    elif len(transform.shape) == 2 and transform.shape == (3, 4) and len(positions.shape) == 2 and \
            positions.shape == (3, 4):
        result = np.zeros_like(positions)
        result[:3, :3] = transform[:3, :3] @ positions[:3, :3]
        result[:3, 3] = transform[:3, :3] @ positions[:3, 3] + transform[:3, 3]
        return result
    else:
        raise NotImplementedError()


def invert_transform(transform):
    if len(transform.shape) == 3 and transform.shape[-2:] == (3, 4):
        result = np.zeros_like(transform)
        result[:, :3, :3] = transform[:, :3, :3].transpose(0, 2, 1)
        result[:, :3, 3:4] = -np.matmul(result[:, :3, :3], transform[:, :3, 3:4])
        return result
    else:
        raise NotImplementedError()


def vdot(a, b):
    result = np.zeros_like(a)
    result[:, 0] = a[:, 1] * b[:, 2] - a[:, 2] * b[:, 1]
    result[:, 1] = a[:, 2] * b[:, 0] - a[:, 0] * b[:, 2]
    result[:, 2] = a[:, 0] * b[:, 1] - a[:, 1] * b[:, 0]
    return result
