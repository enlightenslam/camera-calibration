import numpy as np
import rosbag
from scipy.spatial.transform import Rotation

from .frame_array import FrameArray
from .math import invert_transform


class SetupFromWorldTransformFactory(object):
    @staticmethod
    def make_setup_from_world_transform_factory(rosbag_name, topic_name):
        bagfile = rosbag.Bag(rosbag_name)
        world_from_setup_transforms = []
        timestamps = []
        for topic, msg, t in bagfile.read_messages(topics=[topic_name]):
            world_from_setup_transforms.append(SetupFromWorldTransformFactory.transformation_from_message(msg))
            timestamps.append(msg.header.stamp.to_sec())
        setup_from_world_transforms = invert_transform(np.array(world_from_setup_transforms))
        return FrameArray(setup_from_world_transforms, timestamps)

    @staticmethod
    def transformation_from_message(message):
        matrix = np.zeros((3, 4))
        matrix[0, 3] = message.transform.translation.x
        matrix[1, 3] = message.transform.translation.y
        matrix[2, 3] = message.transform.translation.z
        rotation = message.transform.rotation
        quat = np.array([rotation.x, rotation.y, rotation.z, rotation.w])
        matrix[:3, :3] = Rotation.from_quat(quat).as_matrix()
        return matrix
