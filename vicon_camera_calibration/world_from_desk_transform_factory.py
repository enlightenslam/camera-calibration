import numpy as np
import rosbag

from .frame_array import FrameArray
from .math import vdot


class WorldFromDeskTransformFactory(object):
    @staticmethod
    def make_world_from_desk_transform(marker1_name, marker2_name, marker3_name, rosbag_path, topic_name):
        bagfile = rosbag.Bag(rosbag_path)
        markers = {}
        timestamps = []
        for topic, msg, t in bagfile.read_messages(topics=[topic_name]):
            WorldFromDeskTransformFactory.update_markers(markers, msg)
            timestamps.append(msg.header.stamp.to_sec())
        bagfile.close()
        positions = WorldFromDeskTransformFactory.get_transform_from_markers(np.array(markers[marker1_name]),
                                                                             np.array(markers[marker2_name]),
                                                                             np.array(markers[marker3_name]))
        return FrameArray(positions, timestamps)

    @staticmethod
    def update_markers(markers, message):
        for marker in message.markers:
            position = np.array([marker.translation.x, marker.translation.y, marker.translation.z]) / 1000
            marker_name = marker.marker_name
            if marker_name not in markers.keys():
                markers[marker_name] = []
            markers[marker_name].append(position)

    @staticmethod
    def get_transform_from_markers(marker1, marker2, marker3):
        rotation_x = marker2 - marker1
        rotation_x /= np.linalg.norm(rotation_x, axis=1)[:, None]
        rotation_z = vdot(marker2 - marker1, marker3 - marker1)
        rotation_z /= np.linalg.norm(rotation_z, axis=1)[:, None]
        rotation_y = vdot(rotation_z, rotation_x)
        return np.stack([rotation_x, rotation_y, rotation_z, marker1], axis=2)
